<?php

use PHPUnit\Framework\TestCase;

require_once('app/Models/User.php');

class UserTest extends TestCase
{

    protected $user;

    public function setUp(): void
    {
        $this->user = new App\Models\User;
    }

    public function testGetSetFirstName()
    {
        $this->user->setFirstName("Gerald");

        $this->assertEquals('Gerald', $this->user->getFirstName());
    }

    public function testGetSetLastName()
    {
        $this->user->setLastName("Bullard Jr");

        $this->assertEquals('Bullard Jr', $this->user->getLastName());
    }

    public function testFullNameIsReturned()
    {
        $this->user->setFirstName("Gerald");
        $this->user->setLastName("Bullard Jr");

        $this->assertEquals('Gerald Bullard Jr', $this->user->getFullName());
    }

    public function testFirstLastTrimmed()
    {
        $this->user->setFirstName("Gerald    ");
        $this->user->setLastName("   Bullard Jr");

        $this->assertEquals('Gerald', $this->user->getFirstName());
        $this->assertEquals('Bullard Jr', $this->user->getLastName());
    }

    public function testEmailAddressSet()
    {
        $this->user->setEmail("gerald@mail.com");

        $this->assertEquals("gerald@mail.com", $this->user->getEmail());
    }

    public function testEmailVariables()
    {
        $this->user->setFirstName("Gerald");
        $this->user->setLastName("Bullard Jr");
        $this->user->setEmail("gerald@mail.com");

        $emailVariables = $this->user->getEmailVariables();

        $this->assertArrayHasKey('full_name', $emailVariables);
        $this->assertArrayHasKey('email', $emailVariables);

        $this->assertEquals('Gerald Bullard Jr', $emailVariables['full_name']);
        $this->assertEquals('gerald@mail.com', $emailVariables['email']);
    }

}