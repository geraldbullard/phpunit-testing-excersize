<?php

use App\Calculator\Exceptions\NoOperandsException;
use PHPUnit\Framework\TestCase;

include_once('app/Calculator/Division.php');

class DivisionTest extends TestCase
{

    /** @test
     * @throws NoOperandsException
     */
    public function divideGivenOperands()
    {
        $division = new App\Calculator\Division;
        $division->setOperands(array(100, 2));

        $this->assertEquals(50, $division->calculate());
    }

    /** @test
     * @throws NoOperandsException
     */
    public function removeZeroOperands()
    {
        $division = new App\Calculator\Division;
        $division->setOperands(array(10, 0, 0, 5, 0));

        $this->assertEquals(2, $division->calculate());
    }

    /** @test */
    public function noOperandsThrowException()
    {
        $this->expectException(App\Calculator\Exceptions\NoOperandsException::class);

        $division = new App\Calculator\Division;
        $division->calculate();
    }

}
