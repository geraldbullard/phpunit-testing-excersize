<?php

use PHPUnit\Framework\TestCase;

require_once('app/Calculator/Calculator.php');

class CalculatorTest extends TestCase
{

    /** @test */
    public function canSetSingleOperation()
    {
        $addition = new App\Calculator\Addition;
        $addition->setOperands(array(5, 10));

        $calculator = new App\Calculator\Calculator;
        $calculator->setOperation($addition);

        $this->assertCount(1, $calculator->getOperations());
    }

    /** @test */
    public function canSetMultipleOperations()
    {
        $addition1 = new App\Calculator\Addition;
        $addition1->setOperands(array(5, 10));

        $addition2 = new App\Calculator\Addition;
        $addition2->setOperands(array(2, 2));

        $calculator = new App\Calculator\Calculator;
        $calculator->setOperations(array($addition1, $addition2));

        $this->assertCount(2, $calculator->getOperations());
    }

    /** @test  */
    public function operationsIgnoredIfNotInstanceOfOperationInterface()
    {
        $addition = new App\Calculator\Addition;
        $addition->setOperands(array(5, 10));

        $calculator = new App\Calculator\Calculator;
        $calculator->setOperations(array($addition, 'cats', 'dogs'));

        $this->assertCount(1, $calculator->getOperations());
    }

    /** @test  */
    public function canCalculateResult()
    {
        $addition = new App\Calculator\Addition;
        $addition->setOperands(array(5, 10));

        $calculator = new App\Calculator\Calculator;
        $calculator->setOperations(array($addition));

        $this->assertEquals(15, $calculator->calculate());
    }

    /** @test  */
    public function calculateReturnsMultipleResults()
    {
        $addition = new App\Calculator\Addition;
        $addition->setOperands(array(5, 10)); // 15

        $division = new App\Calculator\Division; // 25
        $division->setOperands(array(50, 2));

        $calculator = new App\Calculator\Calculator;
        $calculator->setOperations(array($addition, $division));

        $this->assertIsArray($calculator->calculate());
        $this->assertEquals(15, $calculator->calculate()[0]);
        $this->assertEquals(25, $calculator->calculate()[1]);
    }

}
