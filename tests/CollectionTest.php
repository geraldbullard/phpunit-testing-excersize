<?php

use PHPUnit\Framework\TestCase;

require_once('app/Support/Collection.php');

class CollectionTest extends TestCase
{

    /** @test */
    public function emptyInstCollNoItems()
    {
        $collection = new App\Support\Collection;

        $this->assertEmpty($collection->get());
    }

    /** @test */
    public function cntCorrectForItems()
    {
        $collection = new App\Support\Collection(array(
            'one', 'two', 'three'
        ));

        $this->assertEquals(3, $collection->count());
    }

    /** @test */
    public function matchedItemsInAndOut()
    {
        $collection = new App\Support\Collection(array(
            'one', 'two'
        ));

        $this->assertCount(2, $collection->get());
        $this->assertEquals('one', $collection->get()[0]);
        $this->assertEquals('two', $collection->get()[1]);
    }

    /** @test */
    public function instanceOfIteratorAggregate()
    {
        $collection = new App\Support\Collection;

        $this->assertInstanceOf(IteratorAggregate::class, $collection);
    }

    /** @test
     * @throws Exception
     */
    public function collCanBeIterated()
    {
        $collection = new App\Support\Collection(array(
            'one', 'two', 'three'
        ));

        $items = array();

        foreach ($collection as $item) {
            $items[] = $item;
        }

        $this->assertCount(3, $items);
        $this->assertInstanceOf(ArrayIterator::class, $collection->getIterator());
    }

    /** @test */
    /*public function collCanBeMergedWithOther()
    {
        $collection1 = new App\Support\Collection(array('one', 'two'));
        $collection2 = new App\Support\Collection(array('three', 'four', 'five'));

        $collection1->merge($collection2);

        $this->assertCount(5, $collection1->get());
        $this->assertEquals(5, $collection1->count());
    }*/

    /** @test */
    public function canAddExistingCollection()
    {
        $collection = new App\Support\Collection(array('one', 'two'));
        $collection->add(array('three'));

        $this->assertEquals(3, $collection->count());
        $this->assertCount(3, $collection->get());
    }

    /** @test */
    public function returnJsonItems()
    {
        $collection = new App\Support\Collection(array(
            array('username' => 'alex'),
            array('username' => 'billy'),
        ));

        $this->assertJson($collection->toJson());
        $this->assertEquals('[{"username":"alex"},{"username":"billy"}]', $collection->toJson());
    }

    /** @test */
    public function jsonEncodeReturnsJson()
    {
        $collection = new App\Support\Collection(array(
            array('username' => 'alex'),
            array('username' => 'billy'),
        ));

        $encoded = json_encode($collection);

        $this->assertJson($encoded);
        $this->assertEquals('[{"username":"alex"},{"username":"billy"}]', $encoded);
    }

}
