<?php

use App\Calculator\Exceptions\NoOperandsException;
use PHPUnit\Framework\TestCase;

include_once('app/Calculator/Addition.php');

class AdditionTest extends TestCase
{

    /** @test
     * @throws NoOperandsException
     */
    public function addGivenOperands()
    {
        $addition = new App\Calculator\Addition;
        $addition->setOperands(array(5, 10));

        $this->assertEquals(15, $addition->calculate());
    }

    /** @test */
    public function noOperandsThrowException()
    {
        $this->expectException(App\Calculator\Exceptions\NoOperandsException::class);

        $addition = new App\Calculator\Addition;
        $addition->calculate();
    }

}