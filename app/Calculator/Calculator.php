<?php

namespace App\Calculator;

include_once('app/Calculator/OperationAbstract.php');
include_once('app/Calculator/OperationInterface.php');
include_once('app/Calculator/Exceptions/NoOperandsException.php');

/*extends OperationAbstract implements OperationInterface*/

class Calculator
{

    protected $operations = array();

    public function setOperation(OperationInterface $operation): array
    {
        $this->operations[] = $operation;
        return $this->operations;
    }

    public function getOperations(): array
    {
        return $this->operations;
    }

    public function setOperations(array $operations): array
    {
        $filteredOperations = array_filter($operations, function ($operation)
        {
            return $operation instanceof OperationInterface;
        });

        $this->operations = array_merge($filteredOperations);
        return $this->operations;
    }

    public function calculate(): array
    {
        if (count($this->operations) > 1)
        {
            return array_map(function($operation)
            {
                return $operation->calculate();
            }, $this->operations);
        }
        return $this->operations[0]->calculate();
    }

}
