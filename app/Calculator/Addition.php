<?php

namespace App\Calculator;

include_once('app/Calculator/OperationAbstract.php');
include_once('app/Calculator/OperationInterface.php');
include_once('app/Calculator/Exceptions/NoOperandsException.php');

use App\Calculator\Exceptions\NoOperandsException;

class Addition extends OperationAbstract implements OperationInterface
{

    /**
     * @throws NoOperandsException
     */
    public function calculate()
    {
        if (count((array)$this->operands) === 0)
        {
            throw new NoOperandsException;
        }
        return array_sum($this->operands);
    }

}
