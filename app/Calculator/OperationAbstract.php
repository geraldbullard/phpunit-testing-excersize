<?php

namespace App\Calculator;

abstract class OperationAbstract
{

    protected $operands;

    public function setOperands($operands)
    {
        $this->operands = $operands;
    }

}
