<?php

namespace App\Calculator;

include_once('app/Calculator/OperationAbstract.php');
include_once('app/Calculator/OperationInterface.php');
include_once('app/Calculator/Exceptions/NoOperandsException.php');

use App\Calculator\Exceptions\NoOperandsException;

class Division extends OperationAbstract implements OperationInterface
{

    /**
     * @throws NoOperandsException
     */
    public function calculate()
    {
        if (count((array)$this->operands) === 0)
        {
            throw new NoOperandsException;
        }

        return array_reduce(array_filter((array)$this->operands), function ($a, $b)
        {
            if ($a !== null && $b !== null)
            {
                return $a / $b;
            }
            return $b;
        }, null);
    }

}
